# --------- PYODIDE:code --------- #
def somme(nombres):
    """
    nombres est un tableau (type list) non vide contenant des entiers (type int) 
    la fonction renvoie la somme des éléments du tableau 
    """
    resultat = 0
    for nb in nombres:
        resultat = resultat + nb
    return resultat


# --------- PYODIDE:tests --------- #

print(somme([1, 0, 1]))
print(somme([1, 2, 3]))
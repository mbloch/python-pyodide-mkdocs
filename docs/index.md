---
author: M. Hainaut
title: 🏡 Accueil
---

Bienvenue sur la page des cours de SNT / NSI du Lycée Marc Bloch

!!! info
     
    Ce site est la nouvelle version de [nsi2024.site](http://nsi2024.site){:target="_blank" }
<!-- Pour retrouver le tuto pyodide-mkdocs : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
   
Si vous voulez conserver certaines pages de ce modèle sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier .pages   
Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite -->
    

😊  Bienvenue !


